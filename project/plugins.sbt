resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/maven-releases/"

// Play plugins
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.9")