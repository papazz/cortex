package repositories

import java.util.UUID

import domain.task.{Task, TaskStatusService}
import java.util.concurrent.ConcurrentHashMap
import collection.JavaConverters._

trait TaskRepositoryComponent {

  val taskRepository: TaskRepository

  trait TaskRepository {

    def fetchAll: Option[String]

    def fetchOne(id: String): Option[Task]

    def create(task: Task): Task

    def delete(id: String): Unit

    def submit(id: String): Unit

    def update(key: String, value: Task): Unit

  }

}

trait TaskRepositoryComponentImpl extends TaskRepositoryComponent {
  override val taskRepository = new TaskRepositoryImpl

  class TaskRepositoryImpl
    extends TaskRepository
      with TaskStatusService {

    val tasks: ConcurrentHashMap[String, Task] = new ConcurrentHashMap[String, Task]

    override def fetchAll: Option[String] = {
      Option(tasks.toString)
    }

    override def fetchOne(id: String): Option[Task] = {
      val task = tasks.asScala.get(id)
      task
    }

    override def create(task: Task): Task = {
      val idValue = {
        UUID.randomUUID.toString
      }
      val createdTask =
        task.copy(id = Option(idValue))
      tasks.put(idValue, createdTask)
      createdTask
    }

    override def delete(id: String): Unit = {
      tasks.remove(id)
    }

    override def submit(key: String): Unit = {
      tasks.asScala += key -> tasks.asScala(key).copy(status = starting)
    }

    override def update(key: String, value: Task): Unit = {
      tasks.asScala += (key -> value)
    }
  }
}
