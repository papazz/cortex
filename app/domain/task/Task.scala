package domain.task

case class Task(
  id: Option[String],
  name: String,
  cpu: Float,
  mem: Float,
  cmd: String,
  status: String)