package domain.task

trait TaskStatusService {

  final val staging: String = "STAGING"

  final val starting: String = "STARTING"

  final val running: String = "RUNNING"

  final val finished: String = "FINISHED"

  final val failed: String = "FAILED"

  final val killed: String = "KILLED"

  final val killing: String = "KILLING"

  final val lost: String = "LOST"

}
