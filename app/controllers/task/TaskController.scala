package controllers.task

import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import services.{TaskServiceComponent}
import domain.task.{Task, TaskStatusService}

trait TaskController extends Controller with TaskStatusService {
  self: TaskServiceComponent =>

  implicit val taskReads = ((__ \ "name").read[String] and
    (__ \ "cpu").read[Float] and
    (__ \ "memory").read[Float] and
    (__ \ "cmd").read[String])(TaskResource)

  implicit val submitReads =
    (__ \ "id").read[String].map(v => SubmitResource(v))

  implicit val killReads =
    (__ \ "id").read[String].map(v => KillResource(v))

  implicit val submitWrites = new Writes[String] {
    override def writes(id: String): JsValue = {
      Json.obj("id" -> id)
    }
  }

  implicit val taskWrites = new Writes[Task] {
    override def writes(task: Task): JsValue = {
      Json.obj("id" -> task.id,
               "name" -> task.name,
               "cpu" -> task.cpu,
               "memory" -> task.mem,
               "command" -> task.cmd)
    }
  }

  def createTask = Action(parse.json) { request =>
    unmarshalJsValue(request) { resource: TaskResource =>
      val task = Task(Option.empty,
                      resource.name,
                      resource.cpu,
                      resource.mem,
                      resource.cmd,
                      staging)
      Ok(Json.toJson(taskService.create(task).id))
    }
  }

  def killTask = Action(parse.json) { request =>
    unmarshalJsValue(request) { resource: KillResource =>
      taskService.kill(id = resource.id)
      Ok
    }
  }

  def submitTask = Action(parse.json) { request =>
    unmarshalJsValue(request) { resource: SubmitResource =>
      taskService.submit(id = resource.id)
      Ok
    }
  }

  def unmarshalJsValue[R](request: Request[JsValue])(block: R => Result)(
      implicit rds: Reads[R]): Result =
    request.body
      .validate[R](rds)
      .fold(valid = block, invalid = e => {
        val error = e.mkString
        Logger.error(error)
        BadRequest(error)
      })
}

case class TaskResource(name: String, cpu: Float, mem: Float, cmd: String)

case class KillResource(id: String)
case class SubmitResource(id: String)
