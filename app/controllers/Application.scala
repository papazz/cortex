package controllers

import play.api.mvc._
import play.api.libs.json._
import controllers.task.TaskController
import services.TaskServiceComponentImpl
import repositories.TaskRepositoryComponentImpl

class Application extends TaskController
  with TaskServiceComponentImpl
  with TaskRepositoryComponentImpl {

  def getAllTasks = Action {
    Ok(Json.toJson(taskService.fetchAll))
  }
}