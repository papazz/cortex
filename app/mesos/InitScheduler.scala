package mesos

import org.apache.mesos._

import scala.concurrent.duration._
import com.typesafe.config._
import org.apache.mesos.Protos.FrameworkInfo

class InitScheduler {

  val config = ConfigFactory.load("../conf/scheduler.conf")

  lazy val frameworkInfo: FrameworkInfo =
    Protos.FrameworkInfo.newBuilder
      .setName(config.getString("scheduler.name"))
      .setFailoverTimeout(config.getInt("scheduler.failoverTimeout").seconds.toMillis)
      .setCheckpoint(false)
      .setUser(config.getString("scheduler.user"))
      .build

  val scheduler = new FusionScheduler
  val mesosMasterHost = config.getString("mesos.master-host") + ":" + config.getString("mesos.master-port")

  val driver: SchedulerDriver =
    new MesosSchedulerDriver(scheduler, frameworkInfo, mesosMasterHost)

  new Thread {
    override def run() {
      try {
        driver.run
      } catch {
        case t: Throwable =>
          driver.stop()
          sys.exit(0)
      }
    }
  }.start()

}
