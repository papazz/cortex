package mesos

import org.apache.mesos._
import org.apache.mesos.Protos._
import scala.collection.mutable
import scala.collection.JavaConverters._
import domain.task.{Task, TaskStatusService}
import java.util.Collections
import org.slf4j.LoggerFactory
import play.api.Logger

class FusionScheduler
  extends Scheduler
    with TaskStatusService {

  private val log = {
    LoggerFactory.getLogger(this.getClass)
  }
  private val tasks: mutable.Queue[Task] = mutable.Queue[Task]()

  override def disconnected(driver: SchedulerDriver): Unit =
    log.info("[scheduler] was disconnected from master")

  override def error(driver: SchedulerDriver, message: String): Unit =
    log.info(s"[error] message: [$message]")

  override def executorLost(driver: SchedulerDriver,
                            executorId: ExecutorID,
                            slaveId: SlaveID,
                            status: Int): Unit =
    log.info(s"[executor] has been lost, Id: [${executorId.getValue}]")

  override def frameworkMessage(driver: SchedulerDriver,
                                executorId: ExecutorID,
                                slaveId: SlaveID,
                                data: Array[Byte]): Unit = {
    log.info(s"[frameWorkMessage]")
  }

  override def offerRescinded(driver: SchedulerDriver,
                              offerId: OfferID): Unit =
    log.info(s"[offer] Id: [${offerId.getValue}] has been rescinded")

  override def registered(driver: SchedulerDriver,
                          frameworkId: FrameworkID,
                          masterInfo: MasterInfo): Unit = {
    val address = masterInfo.getAddress
    log.info(s"[registered] with Mesos master, address: [$address]")
  }

  override def reregistered(driver: SchedulerDriver,
                            masterInfo: MasterInfo): Unit = {
    val address = masterInfo.getAddress
    log.info(s"[reregistered] with Mesos master, address: [$address]")
  }

  override def resourceOffers(driver: SchedulerDriver,
                              offers: java.util.List[Offer]): Unit = {

    for (offer <- offers.asScala) {
      tasks
        .dequeueFirst(value => true)
        .foreach(f = t => {
          t.status match {
            case `starting` =>
              val cmd = {
                CommandInfo.newBuilder().setValue(t.cmd).setUser("root")
              }
              Logger.info(t.status)
              val cpus: Resource = Resource
                .newBuilder
                .setType(Value.Type.SCALAR)
                .setName("cpus")
                .setScalar(Value.Scalar.newBuilder.setValue(t.cpu))
                .setRole("*")
                .build

              val mem: Resource = Resource
                .newBuilder
                .setName("mem")
                .setType(Value.Type.SCALAR)
                .setScalar(Value.Scalar.newBuilder.setValue(t.mem).build())
                .build

              val task: TaskInfo = TaskInfo
                .newBuilder
                .setCommand(cmd)
                .setName(t.name)
                .setTaskId(TaskID.newBuilder.setValue(t.id.get))
                .addResources(cpus)
                .addResources(mem)
                .setSlaveId(offer.getSlaveId)
                .build

              driver.launchTasks(Collections.singleton(offer.getId),
                Seq(task).asJava)
            case _ =>
          }
        })
      driver.declineOffer(offer.getId)
    }
  }

  override def slaveLost(driver: SchedulerDriver, slaveId: SlaveID): Unit =
    log.info("[slave] has been lost Id: [${slaveId.getValue}]")

  override def statusUpdate(driver: SchedulerDriver,
                            status: TaskStatus): Unit = {
    val taskId = status.getTaskId.getValue
    val state = status.getState

    log.debug(s"[task] Id: [$taskId] is in state [$state]")
    Logger.info(s"[task] Id: [$taskId] is in state [$state]")
  }

  def killTask(driver: SchedulerDriver, id: String): Unit = {
    val taskId: TaskID = TaskID
      .newBuilder
      .setValue(id)
      .build
    driver.killTask(taskId)
  }

  def submitTasks(task: Task): Unit = {
    this.synchronized {
      this.tasks.enqueue(task)
    }
  }
}
