package services

import mesos._
import domain.task.Task

trait SchedulerServiceComponent {

  val schedulerService: SchedulerService

  trait SchedulerService {

    def launch(task: Task): Unit

    def killTask(id: String): Unit

  }

}

trait SchedulerServiceComponentImpl extends SchedulerServiceComponent {

  override val schedulerService =
    new SchedulerServiceImpl(new InitScheduler)

  class SchedulerServiceImpl(sched: InitScheduler) extends SchedulerService {

    override def launch(task: Task): Unit = {
      sched.scheduler.submitTasks(task)
    }

    override def killTask(id: String): Unit = {
      sched.scheduler.killTask(sched.driver, id)
    }

  }

}
