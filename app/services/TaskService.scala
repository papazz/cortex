package services

import domain.task.Task
import repositories.TaskRepositoryComponent

trait TaskServiceComponent {

  val taskService: TaskService

  trait TaskService {

    def fetchAll: Option[String]

    def fetchOne(id: String): Option[Task]

    def create(task: Task): Task

    def kill(id: String): Unit

    def submit(id: String): Unit

    def update(id: String, task: Task): Unit

  }

}

trait TaskServiceComponentImpl
    extends TaskServiceComponent
    with SchedulerServiceComponentImpl { self: TaskRepositoryComponent =>

  override val taskService = new TaskServiceImpl

  class TaskServiceImpl extends TaskService {

    override def fetchAll: Option[String] = {
      taskRepository.fetchAll
    }

    override def fetchOne(id: String): Option[Task] = {
      taskRepository.fetchOne(id)
    }

    override def create(task: Task): Task = {
      val createdTask = taskRepository.create(task)
      schedulerService.launch(createdTask)
      createdTask
    }

    override def kill(id: String): Unit = {
      schedulerService.killTask(id)
      taskRepository.delete(id)
    }

    override def submit(id: String): Unit = {
      taskRepository.submit(id)
      val task = taskRepository.fetchOne(id)
      schedulerService.launch(task.get)
    }

    override def update(id: String, task: Task): Unit = {
      taskRepository.update(id, task)
    }

  }

}
