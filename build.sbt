lazy val AppSettings = Seq(

	name := "Fusion",

	organization := "Bitworks",

	version := "1.0-SNAPSHOT",

	scalaVersion := "2.11.8",

	resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
	
	libraryDependencies ++= Seq(
      "org.apache.mesos" % "mesos" % "1.0.1",
			"org.slf4j" % "slf4j-api" % "1.7.21",
			"com.typesafe" % "config" % "1.3.1"
	)
)

dockerExposedPorts in Docker := Seq(9000, 9443)

lazy val root = (project in file("."))
	.settings(AppSettings)
	.enablePlugins(PlayScala)